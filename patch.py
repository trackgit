#!/usr/bin/python

import re
import sys
import os.path

import db
import dbcache
from git import git

_boundary_line_regex = re.compile(r'^---')
_diff_head_regex = re.compile(r'^diff --git a/(.*) b/(.*)$')
_index_line_regex = re.compile(r'^index ([a-f0-9]{7,})\.\.([a-f0-9]{7,}) ')
_all_zeros_regex = re.compile(r'^0+$')

class PatchError(Exception):
    pass

class Patch(object):

    def __init__(self, data):
        self.blobs_pre = set()
        self.blobs_post = set()
        self.missing_files = False
        self.data = data
        self.notes = None
        self._parse(data)

    def _parse(self, data):
        boundary_seen = False
        diff_seen = False
        filename = None
        notes = []
        for line in data.splitlines(True):
            if _boundary_line_regex.match(line):
                boundary_seen = True
                continue
            m = _diff_head_regex.match(line)
            if m:
                diff_seen = True
                # at least the 'pre' filename should exist
                filename = m.group(1)
                continue
            if diff_seen:
                m = _index_line_regex.match(line)
                if m:
                    if not _all_zeros_regex.match(m.group(1)):
                        self.blobs_pre.add(m.group(1))
                        self._check_file(filename)
                    self.blobs_post.add(m.group(2))
                continue
            if boundary_seen:
                notes.append(line)
        if diff_seen:
            self.notes = ''.join(notes)

    def _check_file(self, name):
        if os.path.basename(name) not in dbcache.file_cache:
            self.missing_files = True
            print 'patch for missing file "%s"' % name

    def apply(self):
        output, ret = git('am', '-3', input=self.data)
        if ret != 0:
            # clean up
            git('am', '-3', '--abort')
            git('reset', '--hard')
            raise PatchError()
        return output

    def fast_patch_id(self):
        data = self.data
        try:
            i = data.rfind('\n-- \n')
            data = data[:i+1]
        except IndexError:
            pass
        output = git('patch-id', input=data)[0]
        if not output:
            return
        try:
            patch_id, commit_id = output.split()
            return patch_id
        except ValueError:
            return
