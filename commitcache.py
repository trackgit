import shelve
import time

from git import git

class Commit(object):

    def __init__(self, cdate):
        self.cdate = cdate

    def __cmp__(self, other):
        return cmp(self.cdate, other.cdate)

    def __str__(self):
        return '<Commit %s>' % time.strftime("%c", time.localtime(self.cdate))

    @classmethod
    def scan(cls, sha1):
        cdate = int(git('log', '-1', '--pretty=format:%ct', sha1)[0].strip())
        return cls(cdate)

class CommitCache(object):

    def __init__(self, filename):
        self._db = shelve.open(filename)

    def __getitem__(self, key):
        if key in self._db:
            return self._db[key]
        v = self._db[key] = Commit.scan(key)
        return v
