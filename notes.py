#!/usr/bin/python

import os
import sys
import time
import re
from collections import defaultdict
from sqlalchemy.sql import and_
from sqlalchemy.orm import join

import db
from git import git

_eol_space_re = re.compile('[ \t]+$', re.MULTILINE)

class Notes(object):

    def __init__(self, refname, indexname):
        self._indexname = indexname
        self._env = { 'GIT_INDEX_FILE': '.git/'+indexname }
        self._ref = refname
        self._reset()

    def _reset(self):
        self._cache = defaultdict(list)
        self._index = {}

    def extend(self, sha1, seq):
        for line in seq:
            self.append_line(sha1, line)

    def append_line(self, sha1, line):
        self._cache[sha1].append(line)

    def __contains__(self, sha1):
        return sha1 in self._cache

    def flush(self):
        try:
            os.unlink(self._indexname)
        except OSError:
            pass
        count = len(self._cache)
        input = []
        for cmt_sha1 in self._cache.iterkeys():
            count = count - 1
            sys.stdout.write('%6d\r' % count)
            sys.stdout.flush()
            notes = ''.join(self._cache[cmt_sha1]).strip('\n')
            notes = _eol_space_re.sub('', notes) + '\n'
            blob_sha1 = git('hash-object', '-w', '--stdin', input=notes)[0].strip()
            input.append("100644 %s\t%s\n" % (blob_sha1, cmt_sha1))
        sys.stdout.write('\n')
        git('update-index', '--index-info', input=''.join(input), env=self._env)
        previous, ret = git('rev-parse', self._ref)
        if ret != 0:
            args = []
            previous_arg = []
        else:
            args = ['-p', previous.strip()]
            previous_arg = [previous.strip()]
        tree_sha1 = git('write-tree', env=self._env)[0].strip()
        head_sha1 = git('commit-tree', tree_sha1, *args,
                        **{'input':'Mass annotation by notes.py'})[0].strip()
        git('update-ref', '-m', 'Mass annotation by notes.py',
            self._ref, head_sha1, *previous_arg)
        self._reset()

def split_and_tab(buf):
    ret = []
    for line in str(buf).splitlines():
        ret.append('\t%s\n' % line)
    return ret

def compute_notes(commit, mail, guess_by=None):
    full = []
    terse = []
    if mail.author:
        full.append('From: %s\n' % mail.author)
    if mail.subject:
        full.append('Subject: %s\n' % mail.subject)
    full.append('Message-Id: <%s>\n' % mail.message_id)
    terse.append('Message-Id: <%s>\n' % mail.message_id)
    full.append('Date: %s\n'
                 % time.strftime("%c", time.localtime(mail.post_date)))
    if mail.in_reply_to:
        full.append('In-Reply-To: <%s>\n' % mail.in_reply_to)
    if mail.gmane_id:
        full.append('Archived-At: <http://permalink.gmane.org/gmane.comp.version-control.git/%d>\n' % mail.gmane_id)
        terse.append('Archived-At: <http://permalink.gmane.org/gmane.comp.version-control.git/%d>\n' % mail.gmane_id)
    if len(mail.patch)>0 and mail.patch[0].extra_notes:
        full.append('Extra-Notes:\n')
        full.extend(split_and_tab(mail.patch[0].extra_notes))
    full.append('\n')
    terse.append('\n')
    return full, terse

_merge_re = re.compile("^([a-f0-9]{40}) Merge branch '([^']+)'")
def _redo_pu(full, terse):
    pu_ref = git('rev-parse', 'origin/pu')[0].strip()
    pu_topic = db.session.query(db.Topic).filter(db.Topic.name == 'pu').first()
    if pu_topic:
        full.append_line(pu_ref, 'Pu-Overview:\n')
        full.extend(pu_ref, split_and_tab(pu_topic.cooking_notes))
        full.append_line(pu_ref, '\n')
    for line in git('log', '--first-parent', '--pretty=tformat:%H %s',
                    'origin/master..origin/pu', ret_pipe=True):
        m = _merge_re.match(line)
        if not m:
            continue
        sha1 = m.group(1)
        branch = m.group(2)
        t = db.session.query(db.Topic).filter(db.Topic.name == branch).first()
        if t and t.cooking_notes:
            full.append_line(sha1, 'Pu-Topic:\n')
            full.extend(sha1, split_and_tab(t.cooking_notes))
            full.append_line(sha1, '\n')

def _redo_for_query(full, terse, query, skip_if_exists=False, guess_by=None):
    count = 0
    for cmt, mail in query:
        if skip_if_exists and cmt.sha1 in full:
            continue
        f, t = compute_notes(cmt, mail, guess_by=guess_by)
        if not (f or t):
            continue
        if f:
            full.extend(cmt.sha1, f)
        if t:
            terse.extend(cmt.sha1, t)
        sys.stdout.write('\r%6d' % count)
        sys.stdout.flush()
        count = count + 1
    sys.stdout.write('\n')

def _redo_patches(full, terse):
    # matching by patch-id
    query = (db.session.query(db.Commit, db.Mail)
             .select_from(join(db.Mail, db.Commit,
                               db.Mail.patch_id==db.Commit.patch_id))
             .filter(db.Commit.upstream==True)
             .filter(db.Mail.has_patch==True)
             .order_by(db.Mail.post_date))
    _redo_for_query(full, terse, query)
    # matching by author/date
    query = (db.session.query(db.Commit, db.Mail)
             .select_from(join(db.Mail, db.Commit,
                               and_(db.Mail.author==db.Commit.author,
                                    db.Mail.post_date==db.Commit.adate)))
             .filter(db.Commit.upstream==True)
             .filter(db.Mail.has_patch==True))
    _redo_for_query(full, terse, query, True, guess_by='author,date')

def _redo_all():
    count = 0
    full = Notes('refs/heads/notes/full', 'git-full-notes-index')
    terse = Notes('refs/heads/notes/terse', 'git-terse-notes-index')
    _redo_patches(full, terse)
    _redo_pu(full, terse)
    full.flush()
    terse.flush()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        cmt = db.session.query(db.Commit).filter(db.Commit.sha1==sys.argv[1]).one()
        print compute_notes(cmt)
    else:
        _redo_all()
