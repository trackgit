import db
from sqlalchemy.orm import eagerload

class Cache(object):

    def __init__(self, cls, attr, prefetch=None):
        self.cls = cls
        self.attr = attr
        self._cache = {}
        q = db.session.query(self.attr, self.cls)
        if prefetch:
            q.options(eagerload(prefetch))
        for k,v in q:
            self._cache[k] = v
        self._new = set()

    def __getitem__(self, key):
        return self._cache[key]

    def __contains__(self, key):
        return key in self._cache

    def __setitem__(self, key, value):
        self._cache[key] = value
        self._new.add(value)

    def get(self, key):
        try:
            v = self[key]
        except KeyError:
            v = self.cls(key)
            self[key] = v
        return v

    def flush(self):
        db.session.add_all(self._new)
        self._new = set()
        db.session.flush()

print "generating blob cache ...",
blob_cache = Cache(db.Blob, db.Blob.sha1, db.Blob.newest_commit)
print len(blob_cache._cache)

print "generating file cache ...",
file_cache = Cache(db.Filename, db.Filename.name)
print len(file_cache._cache)

print "generating mail cache ...",
mail_cache = Cache(db.Mail, db.Mail.message_id)
print len(mail_cache._cache)

