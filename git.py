# stolen from curseforge's packager repo

import os
import sys
import subprocess

def execute(executable, *args, **kwargs):
    ret_pipe = kwargs.pop('ret_pipe', False)
    cwd = kwargs.pop('cwd', None)
    input = kwargs.pop('input', None)
    input_pipe = kwargs.pop('input_pipe', subprocess.PIPE)
    if kwargs.pop('stderr', False):
        stderr = subprocess.STDOUT
    else:
        stderr = subprocess.PIPE
    _env = kwargs.pop('env', None)
    if _env:
        env = dict(os.environ)
        env.update(_env)
    else:
        env = None
    process = subprocess.Popen(args=(executable,) + args, shell=False,
                               stdout=subprocess.PIPE, stderr=stderr,
                               stdin=input_pipe, cwd=cwd, env=env)
    if input:
        process.stdin.write(input)
    if ret_pipe:
        return process.stdout
    if process.stdin: # 'None' if input_pipe was set
        process.stdin.close()
    output = process.stdout.read()
    process.wait()
    # sys.stdout.write("(Done)\n")
    # sys.stdout.flush()
    if stderr == subprocess.STDOUT:
        errors = output # not really much we can do here
    else:
        errors = process.stderr.read()
    if process.returncode != 0:
        sys.stdout.write("ERROR %d with %r: %s\n%s\n"
                         % (process.returncode, (executable,)+args, output, errors))
        sys.stdout.flush()
        return errors, process.returncode or 1
    return output, 0

def git(*args, **kwargs):
    return execute('git', '--no-pager', *args, **kwargs)
