import sys
import shelve

from git import git

class PatchIDCache(object):

    def __init__(self, dbfile):
        self._db = shelve.open(dbfile)

    def rebuild(self):
        for line in git('rev-list', '--all', ret_pipe=True):
            sha = line.strip()
            self[sha]

    def __getitem__(self, key):
        if key in self._db:
            return self._db[key]
        patch = git('show', key+'^!')[0]
        v = self._db[key] = git('patch-id', input=patch)[0].split()[0]
        return v

    def items(self):
        return self._db.iteritems()


if __name__ == '__main__':
    c = PatchIDCache(sys.argv[1])
    c.rebuild()
    for k,v in c.items():
        print k, v
