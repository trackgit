#!/usr/bin/python

import sys
import re
import mailbox
import email.Iterators
import email.Parser
from collections import defaultdict

_msg_id_regex = re.compile(r'<([^<>]+)>')
def parse_msg_id(str):
    return _msg_id_regex.search(str).group(1)

parser = email.Parser.Parser()
mbox = mailbox.mbox(sys.argv[1], parser.parse)

mbox_parsed = list(mbox)

by_id = {}
id_map = {}
for msg in mbox_parsed:
    msgid = parse_msg_id(msg['Message-Id'])
    by_id[msgid] = msg
    id_map[msg] = msgid

def detect_reply_id(msg):
    if msg['In-Reply-To']:
        return parse_msg_id(msg['In-Reply-To'])
    if msg['References']:
        refs = ' '.join(msg.get_all('References'))
        ref_ids = [m.group(1) for m in _msg_id_regex.finditer(refs)]
        return ref_ids[-1]

parents = {}
root = None
for msg in mbox_parsed:
    parent_id = detect_reply_id(msg)
    if parent_id:
        parents[msg] = by_id[parent_id]
    else:
        root = msg

children = defaultdict(list)
for msg, parent in parents.iteritems():
    children[parent].append(msg)

def get_text_payload(msg):
    if not msg.is_multipart():
        return msg.get_payload()
    textpart = max(email.Iterators.typed_subpart_iterator(msg), key=len)
    if textpart.is_multipart():
        return textpart.get_payload(0)
    else:
        return textpart.get_payload()

_format_patch_regex = re.compile('.*^---$.*^diff --git', re.MULTILINE|re.DOTALL)
_snip_patch_regex = re.compile('.*^-+ ?(?:8<|>8) ?-+\n(.*^diff --git.*)',
                               re.MULTILINE|re.DOTALL)
def guess_patch_contents(msg):
    p = get_text_payload(msg)
    if _format_patch_regex.match(p):
        msg.set_payload(p)
        return msg.as_string()
    m = _snip_patch_regex.match(p)
    if m:
        msg.set_payload(m.group(1))
        return msg.as_string()
    # no patch found
    return None

def recurse_thread(msg):
    yield msg
    for child in children[msg]:
        for m in recurse_thread(child):
            yield m

for msg in recurse_thread(root):
    p = guess_patch_contents(msg)
    if p:
        print id_map[msg]
        if msg in parents:
            print 'Parent:', id_map[parents[msg]]
