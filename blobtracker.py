#!/usr/bin/python

import sys
import os.path
import email.utils as emu
from sqlalchemy.sql import and_
from sqlalchemy.orm import join

import db
import dbcache
from git import git

class BlobTracker(object):

    def scan_commit(self, sha1, autocommit=True, patch_ids=None):
        if not patch_ids:
            patch_ids = {}
            pipe = git('show', sha1, ret_pipe=True)
            for line in git('patch-id', input_pipe=pipe, ret_pipe=True):
                patch_id, commit_sha1 = line.split()
                patch_ids[commit_sha1] = patch_id
        output = git('log', '-1', '--pretty=format:%aD\t%cD\t%an <%ae>', sha1)[0]
        adate_s, cdate_s, author = output.split('\t', 2)
        try:
            author = author.decode('utf8')
        except UnicodeDecodeError:
            author = author.decode('latin1')
        adate = emu.mktime_tz(emu.parsedate_tz(adate_s))
        cdate = emu.mktime_tz(emu.parsedate_tz(cdate_s))
        commit = db.query(db.Commit).filter(db.Commit.sha1==sha1).first()
        if commit:
            # upstream version is the same as our first shot at application
            commit.upstream = True
        else:
            commit = db.Commit(sha1, cdate, adate, author, patch_ids.get(sha1, None))
            db.session.add(commit)

    def scan_history(self, refs):
        refdata = git('rev-parse', *refs)[0]
        refs = refdata.split()
        boundaries = db.session.query(db.Boundary).all()
        args = refs + ['--not'] + [b.sha1 for b in boundaries]
        print 'reading trees ...'
        count = 0
        for line in git('rev-list', '--no-merges', *args, **{'ret_pipe':True}):
            sys.stdout.write('\r%6d' % count)
            sys.stdout.flush()
            sha1 = line.strip()
            self.scan_commit(sha1, autocommit=False)
            count = count + 1
        print '\nstoring boundaries ...'
        for b in db.session.query(db.Boundary).all():
            db.session.delete(b)
        for r in set(refs):
            db.session.add(db.Boundary(r))
        db.session.commit()

if __name__ == '__main__':
    bt = BlobTracker()
    bt.scan_history(sys.argv[1:])
