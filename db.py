from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Table, Column, Integer, String, Binary, Boolean, \
    ForeignKey, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relation, backref, deferred

DeclarativeBase = declarative_base()

engine = create_engine('postgres://trackgit:trackgit@localhost/trackgit', echo=False)
_Session = sessionmaker(bind=engine)
session = _Session()
query = session.query

class Boundary(DeclarativeBase):
    __tablename__ = 'boundaries'
    sha1 = Column(String(40), primary_key=True)
    def __init__(self, sha1):
        self.sha1 = sha1


class Blob(DeclarativeBase):
    __tablename__ = 'blobs'

    sha1 = Column(String(40), primary_key=True)
    newest_commit_sha1 = Column(String(40), ForeignKey('commits.sha1'))

    newest_commit = relation('Commit', primaryjoin='Blob.newest_commit_sha1==Commit.sha1')

    def __init__(self, sha1):
        self.sha1 = sha1
    def update_contained_in(self, commit):
        if self.newest_commit is None \
                or self.newest_commit.cdate < commit.cdate:
            self.newest_commit = commit


class Filename(DeclarativeBase):
    __tablename__ = 'filenames'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True, index=True)

    def __init__(self, name):
        self.name = name


class Commit(DeclarativeBase):
    __tablename__ = 'commits'

    sha1 = Column(String(40), primary_key=True)
    cdate = Column(Integer, index=True)
    adate = Column(Integer)
    author = Column(String(255))
    patch_id = Column(String(40), index=True)
    upstream = Column(Boolean)

    def __init__(self, sha1, cdate, adate, author, patch_id, upstream=True):
        self.sha1 = sha1
        self.cdate = cdate
        self.adate = adate
        self.author = author
        self.patch_id = patch_id
        self.upstream = upstream
    def __cmp__(self, other):
        return cmp(self.cdate, other.cdate)


class Reference(DeclarativeBase):
    __tablename__ = 'mailrefs'

    id = Column(Integer, primary_key=True)
    mail_id = Column(Integer, ForeignKey('mails.id'), index=True)
    reference_id = Column(String(255))

    def __init__(self, mail_id, reference_id):
        self.mail_id = mail_id
        self.reference_id = reference_id


class Mail(DeclarativeBase):
    __tablename__ = 'mails'

    id = Column(Integer, primary_key=True)
    message_id = Column(String(255), nullable=False, unique=True)
    in_reply_to = Column(String(255))
    post_date = Column(Integer)
    author = Column(String(255))
    subject = Column(String(255))
    gmane_id = Column(Integer)
    has_patch = Column(Boolean)
    stale = Column(Boolean, index=True)
    patch_id = Column(String(40), index=True)
    data = deferred(Column(Binary))

    references = relation('Mail', secondary=Reference.__table__,
                          primaryjoin='Mail.id==Reference.mail_id',
                          secondaryjoin='Mail.message_id==Reference.reference_id',
                          foreign_keys=[Reference.mail_id, Reference.reference_id])

    def __init__(self, message_id):
        self.message_id = message_id


class Patch(DeclarativeBase):
    __tablename__ = 'patches'

    id = Column(Integer, primary_key=True)
    commit_sha1 = Column(String(40), ForeignKey('commits.sha1'))
    mail_id = Column(Integer, ForeignKey('mails.id'))
    extra_notes = Column(Binary)

    commit = relation('Commit', backref='patch')
    mail = relation('Mail', backref='patch')

    def __init__(self, commit, mail_id, extra_notes):
        self.commit = commit
        self.mail_id = mail_id
        self.extra_notes = extra_notes


class Topic(DeclarativeBase):
    __tablename__ = 'topics'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), unique=True, index=True)
    timestamp = Column(Integer)
    mail_id = Column(Integer, ForeignKey('mails.id'))
    cooking_notes = Column(Binary)

    mail = relation('Mail', backref='topic')


DeclarativeBase.metadata.create_all(engine)

if __name__ == '__main__':
    b = Boundary("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
    c = Commit("cccccccccccccccccccccccccccccccccccccccc", 0, 0,
               "A U Thor <author@example.com>",
               "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
    b = Blob("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
    m = Mail()
    p = Patch()
