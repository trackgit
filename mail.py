#!/usr/bin/python

import sys
import re
import time
import random
import mailbox
import email.Iterators
import email.Parser
import email.utils
import email.header
import sqlalchemy
from sqlalchemy.orm import join
import cStringIO as StringIO

import db
import patch
from git import git
from blobtracker import BlobTracker
import dbcache

_msg_id_regex = re.compile(r'<([^<>]+)>')
def _parse_msg_id(str):
    m = _msg_id_regex.search(str)
    if m:
        return m.group(1)

parser = email.Parser.Parser()

def _detect_reply_id(msg):
    if msg['In-Reply-To']:
        return _parse_msg_id(msg['In-Reply-To'])
    if msg['References']:
        refs = ' '.join(msg.get_all('References'))
        ref_ids = [m.group(1) for m in _msg_id_regex.finditer(refs)]
        if ref_ids:
            return ref_ids[-1]

def _get_text_payloads(msg):
    if not msg.is_multipart():
        yield msg.get_payload()
        return
    for part in email.Iterators.typed_subpart_iterator(msg):
        if part.is_multipart():
            yield part.get_payload(0)
        else:
            yield part.get_payload()

_format_patch_regex = re.compile('.*^---$.*^diff --git', re.MULTILINE|re.DOTALL)
_snip_patch_regex = re.compile('.*^-+ ?(?:8<|>8) ?-+\n(.*^diff --git.*)',
                               re.MULTILINE|re.DOTALL)
def _guess_patch_contents(msg):
    for p in _get_text_payloads(msg):
        if _format_patch_regex.match(p):
            msg.set_payload(p)
            return msg.as_string()
    m = _snip_patch_regex.match(p)
    if m:
        msg.set_payload(m.group(1))
        return msg.as_string()
    # no patch found
    return None

def try_patch(m, pp, base_sha1):
    git('checkout', '-f', base_sha1)
    try:
        pp.apply()
    except patch.PatchError:
        return # failed
    pipe = git('show', ret_pipe=True)
    output = git('patch-id', input_pipe=pipe)[0]
    if not output:
        # this means the patch had no diff; e.g., a mode change
        return
    patch_id, commit_id = output.split()
    c = db.session.query(db.Commit).filter(db.Commit.sha1 == commit_id).first()
    if not c:
        new_commit = True
        output = git('log', '-1', '--pretty=format:%aD\t%cD\t%an <%ae>', commit_id)[0]
        adate_s, cdate_s, author = output.split('\t', 2)
        adate = email.utils.mktime_tz(email.utils.parsedate_tz(adate_s))
        cdate = email.utils.mktime_tz(email.utils.parsedate_tz(cdate_s))
        try:
            author = author.decode('utf8')
        except UnicodeDecodeError:
            author = author.decode('latin1')
        c = db.Commit(commit_id, cdate, adate, author, patch_id, False)
        blobtracker.scan_commit_tree(c)
    p = db.Patch(c, m.id, pp.notes)
    db.session.add(p)
    return p

def find_blobs(upstream, blobs):
    commits = []
    for prefix in blobs:
        ret = (db.session.query(db.Blob, db.Commit)
               .filter(db.Blob.newest_commit_sha1 == db.Commit.sha1)
               .filter(db.Blob.sha1.like(prefix+'%'))
               .filter(db.Commit.upstream == upstream)
               .order_by(db.Commit.cdate.desc()).first())
        if not ret:
            print 'blob %s not found?!' % prefix
            break
        commits.append(ret[1])
    else:
        if commits:
            # all blobs found
            cmt = min(commits)
            return cmt

def try_patch_anywhere(msg, m):
    print '*', m.message_id
    pdata = _guess_patch_contents(msg)
    if not pdata:
        return
    pp = patch.Patch(pdata)
    if pp.missing_files:
        m.has_patch = False
        return # probably for another project
    # perhaps we already know about this patch id
    patch_id = pp.fast_patch_id()
    if patch_id:
        cmt = (db.query(db.Commit).filter(db.Commit.patch_id==patch_id)
               .filter(db.Commit.upstream==True).first())
        if cmt:
            print "patch id %s" % patch_id
            print "matches commit %s" % cmt.sha1
            p = db.Patch(cmt, m.id, pp.notes)
            db.session.add(p)
            return p
    # first try on the commit given by the blobs
    cmt = find_blobs(True, pp.blobs_pre)
    if cmt:
        print 'trying canonical commit %s (upstream)' % cmt.sha1
        applied = try_patch(m, pp, cmt.sha1)
        if applied:
            return applied
        # this is just hopeless: it doesn't apply to the commit it should!
        return
    else:
        print "no canonical commit found (upstream)"
    #
    cmt = find_blobs(False, pp.blobs_pre)
    if cmt:
        print 'trying canonical commit %s (local)' % cmt.sha1
        applied = try_patch(m, pp, cmt.sha1)
        if applied:
            return applied
        # this is just hopeless: it doesn't apply to the commit it should!
        return
    else:
        print "no canonical commit found (local)"
    # if we have a parent, try on the parent
    parent = db.session.query(db.Mail).filter(db.Mail.message_id==m.in_reply_to).first()
    if parent and parent.has_patch and parent.patch_id:
        cmt = (db.session.query(db.Commit)
               .filter(db.Commit.patch_id==parent.patch_id)
               .order_by(db.Commit.cdate.desc()).first())
        print 'trying to apply on parent %s' % cmt.sha1
        applied = try_patch(m, pp, cmt.sha1)
        if applied:
            return applied
    else:
        print "no parent commit found"
    # try on origin/master
    sha1 = git('rev-list', '--first-parent', '-1', '--before=%d' % m.post_date,
               'origin/master')[0].strip()
    if not sha1:
        sha1 = 'origin/master'
    print 'trying on master at time of patch (%s)' % sha1
    applied = try_patch(m, pp, sha1)
    if applied:
        return applied
    # same for origin/next
    print 'trying on origin/next'
    applied = try_patch(m, pp, 'origin/next')
    if applied:
        return applied
    # all out of ideas!

_whats_cooking_subject = re.compile(r"^What's cooking in git\.git")
_whats_cooking_category = re.compile(r"^\[(.*)\]$")
_whats_cooking_header = re.compile(r"\* (../[a-zA-Z0-9-]+) \([^)]*\) \d+ commits?")
_whats_cooking_separator = re.compile(r"^(-{5,}|-- )$")

def parse_whats_cooking(msg, mail):
    if not (msg["Subject"] and _whats_cooking_subject.match(msg["Subject"])):
        return
    category = None
    branch = 'pu' # initial part goes on 'pu'
    notes = []
    def _rotate_notes(category, branch, notes):
        if branch:
            t = db.session.query(db.Topic).filter(db.Topic.name==branch).first()
            if not t:
                t = db.Topic()
                t.name = branch
                db.session.add(t)
            t.mail_id = mail.id
            t.cooking_notes = '\n'.join(notes)
        notes = []
        if category:
            notes.append("[%s]" % category)
        return notes
    text = ''.join(_get_text_payloads(msg))
    for line in text.splitlines():
        if _whats_cooking_separator.match(line):
            category = None
            notes = _rotate_notes(category, branch, notes)
            branch = None
            continue
        m = _whats_cooking_category.match(line)
        if m:
            category = m.group(1)
            notes = _rotate_notes(category, branch, notes)
            continue
        m = _whats_cooking_header.match(line)
        if m:
            notes = _rotate_notes(category, branch, notes)
            notes.append(line)
            branch = m.group(1)
            continue
        notes.append(line)


def process_mail(mail):
    msg = parser.parse(StringIO.StringIO(mail.data))
    parse_whats_cooking(msg, mail)
    #patch = try_patch_anywhere(msg, mail)
    #if patch:
    #    mail.patch_id = patch.commit.patch_id
    mail.stale = False


def _query_stale_mail():
    return (db.session.query(db.Mail)
            .filter(db.Mail.stale == True)
            .order_by(db.Mail.post_date.asc(), db.Mail.subject.asc()))

def walk_stale_mail():
    global blobtracker
    blobtracker = BlobTracker()
    count = _query_stale_mail().count()
    for mail in _query_stale_mail():
        print "** %6d\n" % count
        process_mail(mail)
        count = count - 1


def get_mail_by_id(msg_id):
    # Note: use first() because we don't know it exists. The DB
    # guarantees uniqueness anyway.
    try:
        return dbcache.mail_cache[msg_id]
    except KeyError:
        return None

def decode_quoted(s):
    try:
        ret = []
        for s, e in email.header.decode_header(s):
            if e:
                s = s.decode(e)
            ret.append(s)
        return ''.join(ret)
    except UnicodeError:
        return s

_space_regex = re.compile(r'\s+')
def sanitize_single_line(s):
    if s is not None:
        return _space_regex.sub(' ', s)[:255]

_gmane_id_regex = re.compile(r'<http://permalink\.gmane\.org/gmane\.comp\.version-control\.git/(\d+)>')
def insert_mail_into_db(msg):
    if (msg.get('Message-Id', None)
        and get_mail_by_id(_parse_msg_id(msg['Message-Id']))):
        return # already exists
    gmane_id = None
    if msg['Archived-At']:
        m = _gmane_id_regex.match(msg['Archived-At'])
        if m:
            gmane_id = int(m.group(1))
    msgid = msg.get('Message-Id', None)
    if not msgid or not _parse_msg_id(msgid):
        if gmane_id:
            msgid = 'gmane-%d@mailnotes.thomasrast.ch' % gmane_id
        else:
            msgid = 'fallback-%X@mailnotes.thomasrast.ch' % random.randrange(2**32)
    else:
        msgid = _parse_msg_id(msgid)
    mail = dbcache.mail_cache.get(msgid)
    mail.gmane_id = gmane_id
    mail.message_id = sanitize_single_line(msgid)
    if msg['From']:
        name, addr = email.utils.parseaddr(msg['From'])
        if name and addr:
            mail.author = sanitize_single_line("%s <%s>" % (decode_quoted(name), addr))
        else:
            mail.author = sanitize_single_line(decode_quoted(msg['From']))
    tm = None
    if msg['Date']:
        tm = email.utils.parsedate_tz(msg['Date'])
    if tm:
        tm = email.utils.mktime_tz(tm)
    else:
        tm = time.time()
    mail.post_date = tm
    if msg['Subject']:
        mail.subject = sanitize_single_line(decode_quoted(msg['Subject']))
    mail.in_reply_to = sanitize_single_line(_detect_reply_id(msg))
    mail.data = msg.as_string()
    mail.stale = mail.has_patch = bool(_guess_patch_contents(msg))
    if msg['Subject'] and _whats_cooking_subject.match(msg['Subject']):
        mail.stale = True
    db.session.add(mail)
    starter = mail
    if mail.in_reply_to:
        parent = get_mail_by_id(mail.in_reply_to)
        if parent:
            starter = parent
    # Flag all so-far unapplied patches downwards of this one as
    # 'stale' so they'll be tried again.  XXX should use an sql UPDATE
    # here!
    for child in (db.session.query(db.Mail)
                  .select_from(join(db.Mail, db.Reference,
                                         db.Mail.id == db.Reference.mail_id))
                  .filter(db.Reference.reference_id == starter.message_id)
                  .filter(db.Mail.has_patch == True)
                  .filter(db.Mail.patch_id == None)
                  .filter(db.Mail.message_id != mail.message_id)
                  .filter(db.Mail.stale == False)):
        child.stale = True
    if msg['References']:
        for m in _msg_id_regex.finditer(' '.join(msg.get_all('References'))):
            db.session.add(db.Reference(mail.id,
                                        sanitize_single_line(m.group(1))))

def _parse_mail(s):
    return parser.parse(StringIO.StringIO(s))

def parse_mbox(fname):
    mbox = open(fname)
    mbox_parsed = []
    cur = None
    for line in mbox:
        if line.startswith('From news@gmane.org'):
            if cur:
                mbox_parsed.append(_parse_mail(''.join(cur)))
            cur = []
        else:
            cur.append(line)
    if cur:
        mbox_parsed.append(_parse_mail(''.join(cur)))
    count = len(mbox_parsed)
    for msg in mbox_parsed:
        insert_mail_into_db(msg)
        count = count - 1
        sys.stderr.write("%6d\r" % count)
    sys.stderr.write("\n")
    dbcache.mail_cache.flush()

if __name__ == '__main__':
    for mbox in sys.argv[1:]:
        parse_mbox(mbox)
    walk_stale_mail()
    db.session.commit()

