#!/bin/sh

# config
GMANE_DROPBOX=~/dl/gmane-git
REPO=~/tmp/git
TOOLPATH=~/dev/trackgit

# download new mails
cd "$GMANE_DROPBOX"
cur_inbox=$(ls | sort -n | tail -1)
cur_gmane_id () {
    grep '^Archived-At' "$1" | tail -1 | sed 's#.*/\([0-9]\{1,\}\)>$#\1#'
}
begin=$(cur_gmane_id $cur_inbox)
boundary=$(($begin+1000))
wget -O gmane-$boundary.tmp http://download.gmane.org/gmane.comp.version-control.git/$begin/$boundary
end=$(cur_gmane_id gmane-$boundary.tmp)
if [ -z "$end" ]; then
    rm gmane-$boundary.tmp
else
    mv gmane-$boundary.tmp $end
fi

# rest _must_ run in git clone
cd "$REPO"

# update our git.git
git fetch origin
"$TOOLPATH"/blobtracker.py origin/master origin/next origin/pu

# import mails
if [ -n "$end" ]; then
    "$TOOLPATH"/mail.py "$GMANE_DROPBOX"/$end
fi

# redo notes
"$TOOLPATH"/notes.py

unwind_noop () {
    git diff-tree --quiet --exit-code $1^! &&
    git update-ref refs/heads/$1 $1^
}
unwind_noop notes/full
unwind_noop notes/terse

# push
git push trast notes/terse notes/full
